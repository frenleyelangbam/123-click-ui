<?php

	session_start();

	include 'db.php';

	$jour = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");

	if(isset($_POST['submit'])){

		$last_id=$_POST['last_id'];
		$worker=$_POST['worker'];
		$comment=$_POST['comment'];    
		$upd_add=mysqli_query($con,"update add_booking_new set worker_id='$worker', color='#00A058' where booking_id='$last_id' ");
		
		if (!empty($comment)) {
			$ins_com=mysqli_query($con,"insert into comment (last_id,comment) values('$last_id','$comment')");
		}
		   
		header('location:index.php');
	}

	if(isset($_POST['delete'])){

		$last_id=$_POST['last_id'];    
		$del_com=mysqli_query($con,"delete from add_booking_new where booking_id='$last_id'");
		header('location:index.php');
	}
?>

<html lang="fr">
	
<head>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="special_detail_new.css" />
	
	<?php include 'header.php'; ?>

    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/2/common.js"></script>
    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/2/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/29/2/stats.js"></script>
    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps/api/js/AuthenticationService.Authenticate?1shttp%3A%2F%2Flocalhost%2F123_CLIC%2Findex.php&amp;callback=_xdc_._kup6ee&amp;token=72873"></script>
</head>

<body>
	<div id="vfx_loader_block" style="display: none;">
  		<div class="vfx-loader-item"> <img src="images/loading.gif" alt=""> </div>
	</div>
	
	<?php include ('menu.php'); ?>

	<?php 
		$last_id=$_GET['last_id'];

		$sql=mysqli_query($con,"select * from add_booking_new where booking_id='$last_id' ");
		$sql_data=mysqli_fetch_array($sql);
		$spe_id=$sql_data['spe_id'];
		$service_id=$sql_data['service_id'];
		$worker_id=$sql_data['worker_id'];

		$data1=mysqli_query($con,"select * from specialist where spr_id='$spe_id' ");
		$data_ar1=mysqli_fetch_array($data1);

		//$data2=mysqli_query($con,"select * from service where serve_id='$service_id' ");
		$data2=mysqli_query($con,"select * from service where serv_title='$service_id' ");
		$data_ar2=mysqli_fetch_array($data2);
		//$serv_id=$data_ar2['serve_id'];
		$service_price=$data_ar2['price'];
		$service_time=$data_ar2['time'];

		$data3=mysqli_query($con,"select * from worker where work_id='$worker_id' ");
		$data_ar3=mysqli_fetch_array($data3);

		//$data34=mysqli_query($con,"select * from service_detail where ser_detail_id='$serv_id' ");
		$data34=mysqli_query($con,"select * from service_detail where ser_detail_id='$service_id' ");
		$data_ar33=mysqli_fetch_array($data34);

		$worker_id_get1=mysqli_query($con,"select * from worker where spe_id='$spe_id' ");
		$service_worker_avl=[];
		$worker_without_holiday=[];
		$current_date=date('m/d/Y',strtotime($sql_data['date_booking']));
		$time_booking11=$sql_data['time_booking'];

		while($worker_id_array1=mysqli_fetch_array($worker_id_get1)){

			$worker12=$worker_id_array1['work_id'];
			$worker_service=mysqli_query($con,"select * from worker_service where worker_service_id='$worker12' and service_id='$service_id' ");
			$worker_id_with_service=mysqli_fetch_array($worker_service);
			$worker_id=$worker_id_with_service['worker_service_id'];
			$service_worker_avl[].=$worker_id_with_service['worker_service_id'];


			$worker_holiday=mysqli_query($con,"select * from worker_holiday where worker_id='$worker_id' ");
			$data1=mysqli_fetch_array($worker_holiday);  
			$query1 = "";

			if(!empty($data1['day_leave_start']) || !empty($data1['day_leave_end'])){

				$day_leave_start=$data1['day_leave_start'];
				$day_leave_end=$data1['day_leave_end'];
				$query1 = "BETWEEN '$day_leave_start' and '$day_leave_end'";
				$worker_holiday1=mysqli_query($con,"select * from worker_holiday where worker_id='$worker12' and  '$current_date' $query1 ");
			}  

			$worker_id_without_hoiliday=mysqli_fetch_array($worker_holiday1);
			$worker_without_holiday[].=$worker_id_without_hoiliday['worker_id'];

		  }
  
		  $worker_available = array_diff($service_worker_avl, $worker_without_holiday);
		  $current_date11=date('Y-m-d',strtotime($sql_data['date_booking']));
		  $worker_id_get=mysqli_query($con,"select worker_id from add_booking_new where spe_id='$spe_id' and status='1' and date_booking='$current_date11' and time_booking='$time_booking11'" );
		  $add_booking_worker=[];
  
		  while($worker_id_array=mysqli_fetch_array($worker_id_get)){
			$add_booking_worker[].=$worker_id_array['worker_id'];
		  }

		  $result = array_diff($worker_available, $add_booking_worker);
		  $get_id=implode(',', $result);
		?>

		<div id="search-categorie-item">
		  <div class="container">
			<div class="row">
			  <div class="col-sm-12 text-center">
				<div class="row">
				  <div class="col-md-12 categories-heading bt_heading_3">
				   	<h1 style="color: #850035;">Détails du rendez-vous</span></h1>
					<div class="blind line_1"></div>
					<div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
					<div class="blind line_2"></div>
				  </div>
				  <div class="Specialist_block">
          			<form method="post">
						<div class="madicament_sec">
            				<div class="col-md-6">
              			 	  <div class="col-md-5">
								<span><img src="../directory_admin/uploads/<?php echo $data_ar1['brand_logo']; ?>" class="inside"></span>	
							  </div>
							  <div class="col-md-5">	
								<h2><?php echo $data_ar1['brand_name']; ?></h2>
								<ul>
									  <li><?php echo $data_ar1['disc']; ?></li>
									  <li><?php echo utf8_encode($data_ar1['address']); ?></li>
									  <li><?php echo utf8_encode($data_ar1['address_map']); ?></li>	  
									  <li> Tel: <?php echo $data_ar1['contact']; ?></li>
								</ul>
							  </div>	
							</div>
						</div>
						<h3> Service </h3>
						<p><?php echo ucfirst($data_ar33['ser_name']); ?></p>

						<p><select class="form-control" name="worker" style="width: 20%;margin-left: 40%;" >
            				<?php 
            
								if(empty($get_id)) {
								  echo '<option value="">Aucun employe</option>';
								}else{ 
								  echo '<option value="">Choix employé</option>';

								  $sql1=mysqli_query($con,"SELECT * from worker Where work_id IN ($get_id)");

								  while($worker_data=mysqli_fetch_array($sql1)) {?>

									<option value="<?php echo $worker_data['work_id']; ?>"><?php  echo $worker_data['first_name']; ?>&nbsp;<?php  echo $worker_data['last_name']; ?></option>
              				<?php  } }?>
          
						</select></p style="height: 83px;">
						
						<p><textarea class="form-control" id="exampleTextarea" name="comment" rows="8" placeholder="Vous pouvez saisir un commentaire pour le rendez-vous..." style="margin-top: 1%;"" ></textarea>
          				</p>

						<h3> Date et Heure: </h3>
						
						<p>
							<?php
							 echo $jour[date("w", strtotime($sql_data['date_booking']))]."&nbsp;"; $da=$sql_data['date_booking'];
							 echo date("d F Y", strtotime($da)); ?> à 
							 <?php
								  echo $time_show=$_SESSION['time_show'];
								  //date('h:i', strtotime($ti));
							  ?> 
						 </p>
							
            			<h3> Durée estimée: <?php echo $service_time; ?> min  /  Prix: <?php echo $service_price; ?>€ TTC</h3>
						                             
						<input type="hidden" name="last_id" value="<?php echo $last_id; ?>">
						<button type="submit" name="delete"> Annuler </button>
						<button type="submit" name="submit"> Confirmer </button>
           			 </form>
          			</div>
          			<hr>
				</div>
			  </div>
			</div>
		  </div>
		</div>
							
<?php include ('footer.php'); ?>
							
</body>
							
</html>