<script type="text/javascript">
  function getsubcat(val) {
        document.getElementById('value').value=val;
        $.ajax({
        type: "POST",
        url: "get_subcat.php",
        data: "cat_id=" + val,
        success: function (response){ 
         response1=JSON.parse(response);
         if(response1.chk=='true'){
            $('#subcat').html(response1.subcategory);
            document.getElementById("subcat").disabled = false;
         }else{
            document.getElementById("subcat").disabled = true;
            document.getElementById("subcat").options.length=0;
            var sub=0;
                  $.ajax({
                  type: "POST",
                  url: "get_service.php",
                  data: "subcat_id="+sub+"&cat_id="+val,
                  success: function (response){   
                  console.log(response); 
                  $('#service').html(response);
                  }
                  });                  
         }
        }
        });
  }

  function getservice(val) {
        var cat_id=document.getElementById('value').value;
        $.ajax({
        type: "POST",
        url: "get_service.php",
        data: "subcat_id=" + val+ "&cat_id="+cat_id,
        success: function (response){   
        console.log(response); 
         $('#service').html(response);
        }
        });
        }
</script>
<div id="location-map-block" style="display: none;">
  <div id="location-homemap-block"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-lg-12">
        <div id="location-link-item">
          <button id="map_list"><i class="fa fa-angle-double-up"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="slider-banner-section" style="height: 300PX; display: block;">
  <div class="container">
    <div class="row" style="margin-top: 11px;">
      <div class="col-sm-12 text-center">
        <div class="col-sm-3 col-xs-9">
        <div id="logo"> <a href="index.php"><img src="images/logo.png" alt="logo" style="height: 100px"></a> </div>
      </div>
      <div class="col-sm-9 text-right">
        <nav class="navbar navbar-default">
          <div class="navbar-header" style="margin-top: 24px;">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#thrift-1" aria-expanded="false"> <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          <div class="collapse navbar-collapse" id="thrift-1"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
            <div id="nav_menu_list">
              <ul>
                <li><a href="aboutpro.php"> <i class="fa fa-space-shuttle" aria-hidden="true"></i><br/>Professionnel</a></li>
               <?php if (!isset($_SESSION['email'])) { ?>
                <span class="btn_item">
                <li>
                  <button class="btn_login" data-toggle="modal" data-target="#login">S'identifier</button>
                </li>
                <li>
                  <button class="btn_register" data-toggle="modal" data-target="#register">S'enregistrer</button>
                </li>
                </span>
                <?php } else {?>
                <li class=""><a href="fav_list.php"> <i class="fa fa-heart" aria-hidden="true"></i><br/>Mes Favoris</a></li>
                <span class="btn_item">
                <!--   <li><a href="#"> <i class="fa fa-user" aria-hidden="true"></i><br/>My Account</a></li> -->
                <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="margin-bottom: -26%"><i class="fa fa-user" aria-hidden="true" ></i><br/>Mon compte<span class="caret"></span></a>
                <ul class="dropdown-menu" style="">
                <li><a data-toggle="modal" data-target="#profile1" href="#" style="color: black;">Mes informations</a></li>
                <li><a href="appointment.php" style="color: black;">Mes Rendez-vous</a></li>
	      <li><a href="logout.php" style="color: black;">Se d&eacute;connecter</a></li>
                </ul>
                </li>
                <!-- <li>
                 <a href="logout.php" >Logout</a>
                </li> -->              
                </span>
                <?php } ?>
              </ul>
            </div>
          </div>
        </nav>
      </div>
        <!-- <div id="home-slider-item"> <span class="helpyou_item">We <span>Help</span> You to</span>
          <h1>AMAZING <span>CLASSIFIED</span> HTML <span>TEMPLATE</span></h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
        </div> -->
        <div id="search-categorie-item-block" style="margin: 50px 100px;">
          <form method="POST" action="search.php">
            <!-- <h1>search any business listing</h1> -->
            <div class="col-sm-9 col-md-10 nopadding">
              <div id="search-input">
              <div class="col-md-4 nopadding">
              <input type="hidden" name="cat" value="" id="value">
                  <select id="location-search-list" name="cat" class="form-control" style="border-radius:unset;" required="" onchange="getsubcat(this.value);">
                    <option value="">Choisir une cat&eacute;gorie</option>
                   <?php 
                  $data=mysqli_query($con,"select * from category");
                  while($data_ar=mysqli_fetch_array($data)){
                    ?>
                    <option value="<?php echo $data_ar['cat_id']; ?>"><?php echo $data_ar['name']; ?></option>
                    <?php }  ?>
                  </select>
                </div>
                <!-- <div class="col-md-3 nopadding">
                  <select id="subcat" name="subcat" class="form-control" style="border-radius:unset;" onchange="getservice(this.value)">
                    <option>Sélectionnez la sous-catégorie</option>
                  </select>
                </div> -->  
                <div class="col-md-4 nopadding">
                  <select id="subcat" name="subcat" class="form-control" style="border-radius:unset;">
                    <option>Selectionnez la sous-cat&eacute;gorie</option>
                  </select>
                </div>
               <!--  <div class="col-md-3 nopadding">
                  <select id="service" class="form-control" name="service" style="border-radius:unset;" required="">
                    <option value="">Sélectionnez la sous-catégorie</option>
                  </select>
                </div> -->
                <div class="col-md-4 nopadding">
                  <input type="text" name="textdata" class="form-control" placeholder="Entrez code postal ou ville" >
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-md-2 text-right nopadding-right">
              <div id="location-search-btn" style="margin-right: 140px;margin-left: -15px;">
              <?php if (!isset($_SESSION['email'])) { ?>
                <button type="button" data-toggle="modal" data-target="#login"   style="background-color: #9a0440;"><i class="fa fa-search"></i></button>
                <?php }else{ ?>
                <button type="submit" name="submit"  style="background-color: #9a0440;"><i class="fa fa-search"></i></button>
                <?php } ?>
              </div>
            </div>
          </form>
        </div>
        <!-- <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div> -->
      </div>
    </div>
  </div>
</div>